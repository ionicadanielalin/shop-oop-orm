<!DOCTYPE html>
<?php include "includes/parts/header.php";
include "includes/functions.php";

?>

<body>
<section>
    <div class="container">
        <div class="row">
            <?php include "includes/parts/sidebar.php";
            $category = new Category($_GET['category']);
            showCategoryPage($category->getProducts());
            ?>
        </div>
    </div>
</section>
</body>

<?php include "includes/parts/footer.php"
?>
</html>