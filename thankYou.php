<?php
session_start();
include "includes/parts/header.php";
include "includes/functions.php";

?>

<div class="jumbotron text-center">
  <h1 class="display-3">Thank You for shopping from us :) !</h1>
  <p class="lead"><strong>Please check your email</strong> for further instructions on how to follow your order status.</p>
  <hr>
  <p>
Having any trouble ? <strong>:(</strong> <br> <a href="">Contact us</a>
  </p>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="index.php" role="button">Continue to homepage</a>
  </p>
</div>

<?php
unset($_SESSION['cart_id']);

//header("Location: index.php");

?>