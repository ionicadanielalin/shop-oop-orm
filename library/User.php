<?php


class User extends BaseEntity
{
    const STATUS_UNCONFIRMED = 'unconfirmed';

    const STATUS_ACTIVE = 'active';

    const STATUS_INACTIVE = 'inactive';

    public $username;

    public $password;

    public $email;

    public $status = self::STATUS_UNCONFIRMED;

    public function save()
    {
        $pass = $this->password;
        $this->password = md5($this->password);
        parent::save();
        $this->password=$pass;
    }
}